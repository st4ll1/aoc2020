package main

import "core:fmt"
import "core:strings"
import "core:strconv"
import "core:os"
import "core:math"
import "core:sort"

main :: proc ()
{
    if len(os.args) > 1
    {
        switch os.args[1] {
            case "1": day1();
            case "2": day2();
            case "3": day3();
            case "4": day4();
            case "5": day5();
            case "6": day6();
            case "7": day7();
            case "8": day8();
            case "9": day9();
            case "10": day10();
            case "11": day11();
            case "12": day12();
            case: fmt.printf("day %s not implemented", os.args[1]);
        }
    }
    else
    {
        //day1();
        //day2();
        //day3();
        //day4();
        //day5();
        //day6();
        //day7();
        //day8();
        //day9();
        //day10();
        day11();
        //day12();
    }
}

load_input :: proc (file : string) -> []string
{
    data, success := os.read_entire_file(file);
    if !success {
        fmt.printf("error opening file: %s", file);
        os.exit(1);
    }
    return  strings.split(strings.trim_null(string(data)), "\n");
}

day12 :: proc ()
{
    fmt.printf("Day 12:\n");
    lines := load_input("input/12");
    //lines := load_input("input/12_test");

    using Direction :: enum {EAST = 0, SOUTH = 1, WEST = 2, NORTH = 3 };
    using Turn :: enum {RIGHT, LEFT};

    Point :: struct {
        north : int,
        east : int,
    };

    waypoint : Point = {1, 10};
    ship2: Point = {0, 0};

    current_direction := EAST;

    east := 0;
    north := 0;

    action : u8 = 'E';
    value := 0;


    waypoint_turn :: proc (turn : Turn, value : int, waypoint: Point) -> Point
    {
        v := value / 90;
        wp : Point;
        if turn == Turn.LEFT
        {
            v = -v;
        }
        v = v %% 4;
        switch v
        {
            case 1:
                wp.east = waypoint.north;
                wp.north = -waypoint.east;
            case 2:
                wp.north = -waypoint.north;
                wp.east = -waypoint.east;
            case 3:
                wp.east = -waypoint.north;
                wp.north = waypoint.east;
        }
        return wp;
    }

    waypoint_move :: proc (direction : Direction, value : int, waypoint : Point)
    -> Point
    {
        wp := waypoint;
        switch direction
        {
            case Direction.EAST:
                wp.east += value;
            case Direction.SOUTH:
                wp.north -= value;
            case Direction.WEST:
                wp.east -= value;
            case Direction.NORTH:
                wp.north += value;
        }
        return wp;
    }

    ship_forward :: proc (value: int, waypoint: Point, ship: Point) -> Point
    {
        p := ship;
        p.north += value * waypoint.north;
        p.east += value * waypoint.east;
        return p;
    }

    turn :: proc (turn : Turn, value : int, current_direction : ^Direction)
    {
        v := value / 90;
        if turn == Turn.LEFT
        {
            v = -v;
        }
        current_direction^ = Direction((int(current_direction^) + v) %% 4);
    }

    move :: proc (direction : Direction, value: int, east: ^int, north: ^int)
    {
        switch direction
        {
            case Direction.EAST:
                east^ += value;
            case Direction.SOUTH:
                north^ -= value;
            case Direction.WEST:
                east^ -= value;
            case Direction.NORTH:
                north^ += value;
        }
    }

    for line in lines
    {
        if len(line) == 0 {continue;}
        action = line[0];
        value = strconv.atoi(line[1:]);
        fmt.printf("action: %c, value: %d\n", action, value);

        switch action
        {
            case 'E':
                move(EAST, value, &east, &north);
                waypoint = waypoint_move(EAST, value, waypoint);
            case 'W':
                move(WEST, value, &east, &north);
                waypoint = waypoint_move(WEST, value, waypoint);
            case 'N':
                move(NORTH, value, &east, &north);
                waypoint = waypoint_move(NORTH, value, waypoint);
            case 'S':
                move(SOUTH, value, &east, &north);
                waypoint = waypoint_move(SOUTH, value, waypoint);
            case 'F':
                move(current_direction, value, &east, &north);
                ship2 = ship_forward(value, waypoint, ship2);
            case 'R':
                turn(RIGHT, value, &current_direction);
                waypoint = waypoint_turn(RIGHT, value, waypoint);
            case 'L':
                turn(LEFT, value, &current_direction);
                waypoint = waypoint_turn(LEFT, value, waypoint);
        }
        fmt.println("waypoint: ", waypoint);
        fmt.println(ship2);
    }
    fmt.printf("Part1: %d + %d = %d\n", abs(east), abs(north), abs(east) + abs(north));
    fmt.printf("Part2: %d + %d = %d\n", abs(ship2.east), abs(ship2.north),
    abs(ship2.east) + abs(ship2.north));
}

day11 :: proc ()
{
    fmt.printf("Day 11:\n");
    lines := load_input("input/11");
    //lines := load_input("input/11_test");

    width := len(lines[1]);
    height := len(lines)-1;

    using State :: enum { FLOOR, EMPTY, TAKEN };

    f := make([]State, width * height * 4);
    defer delete(f);

    round := 0;
    changed : bool = true;
    changed2 : bool = true;

    for line,y in lines
    {
        if len(line) == 0 {continue;}
        for ch,x in line
        {
            switch ch {
                case 'L':
                    f[width * y + x] = EMPTY;
                    f[width * y + x + (2*width*height)] = EMPTY;
                case '.':
                    f[width * y + x] = FLOOR;
                    f[width * y + x + (2*width*height)] = FLOOR;
                case '#':
                    f[width * y + x] = TAKEN;
                    f[width * y + x + (2*width*height)] = TAKEN;
            }
        }
    }

    // for visual debugging
    print_field :: proc (f: ^[]State, width:int, height:int)
    {
        spot :: proc (f: ^[]State, index : int)
        {
                switch f[index]
                {
                    case State.EMPTY:
                        fmt.print("L");
                    case State.FLOOR:
                        fmt.print(".");
                    case State.TAKEN:
                        fmt.print("#");
                }
                fmt.print(" ");
        }
        for y:=0; y<height; y+=1
        {
            for x:=0; x<width; x+=1
            {
                spot(f, x + y * width);
            }
            fmt.print("    ");
            for x:=0; x<width; x+=1
            {
                spot(f, x + y * width + width * height);
            }
            fmt.print("    ");
            for x:=0; x<width; x+=1
            {
                spot(f, x + y * width + (2 * width * height));
            }
            fmt.print("    ");
            for x:=0; x<width; x+=1
            {
                spot(f, x + y * width + (3 * width * height));
            }
            fmt.print("    \n");

        }
        fmt.print("\n\n");
    };

    Direction :: struct
    {
        x : int,
        y : int,
    };

    offset := 0;
    offset_target := 0;
    offset_1 := 0;
    offset_2 := 0;
    ix := 0;
    iy := 0;
    count := 0;
    for ;changed || changed2;round+=1
    {
        offset = width * height * (round % 2);
        offset_target = width * height * ((round + 1) % 2);
        offset_1 = offset + 2 * width * height;
        offset_2 = offset_target + 2 * width * height;
        changed = false;
        changed2 = false;
        for i:=0;i<width*height;i+=1
        {
            ix = i%width;
            iy = i/width;
            //part 1
            count = 0;
            for y:=iy-1;y<=iy+1;y+=1
            {
                if y < 0 {continue;}
                if y >= height {continue;}
                for x:=ix-1;x<=ix+1;x+=1
                {
                    if x < 0 {continue;}
                    if x >= width {continue;}
                    if x == ix && y == iy {continue;}

                    //fmt.println(x,  y, width, offset);
                    if f[x + y * width + offset] == State.TAKEN
                    {
                        count += 1;
                    }
                }
            }
            if count == 0 && f[i + offset] == State.EMPTY
            {
                f[i + offset_target] = State.TAKEN;
                changed = true;
            }
            else if count >= 4 && f[i + offset] == State.TAKEN {
                f[i + offset_target] = State.EMPTY;
                changed = true;
            }
            else
            {
                f[i + offset_target] = f[i + offset];
            }
            // part 2

            dir_count :: proc (ix:int, iy: int, f: ^[]State, offset:int, width:
            int, height: int, count: int, dir: Direction) -> int
            {
                for t:=1;true;t+=1
                {
                    x := ix + t * dir.x;
                    y := iy + t * dir.y;
                    if x < 0 || y < 0 || x >= width || y >= height {break;}
                    switch f[x + y * width + offset]
                    {
                        case State.TAKEN:
                            return count + 1;
                        case State.EMPTY:
                            return count;
                        case State.FLOOR:
                            continue;
                        case:
                            continue;
                    }
                }
                return count;
            }

            count2 := 0;
            // make a proc
            count2 = dir_count(ix, iy, &f, offset_1, width, height, count2, {1,0});
            count2 = dir_count(ix, iy, &f, offset_1, width, height, count2, {1,1});
            count2 = dir_count(ix, iy, &f, offset_1, width, height, count2, {0,1});
            count2 = dir_count(ix, iy, &f, offset_1, width, height, count2, {-1,0});
            count2 = dir_count(ix, iy, &f, offset_1, width, height, count2, {-1,-1});
            count2 = dir_count(ix, iy, &f, offset_1, width, height, count2, {0,-1});
            count2 = dir_count(ix, iy, &f, offset_1, width, height, count2, {1,-1});
            count2 = dir_count(ix, iy, &f, offset_1, width, height, count2, {-1,1});
            //fmt.println("count2:", count2);
            //dir : Direction = { 1, 0 };
            if count2 == 0 && f[i + offset_1] == State.EMPTY
            {
                f[i + offset_2] = State.TAKEN;
                changed2 = true;
            }
            else if count2 >= 5 && f[i + offset_1] == State.TAKEN {
                f[i + offset_2] = State.EMPTY;
                changed2 = true;
            }
            else
            {
                f[i + offset_2] = f[i + offset_1];
            }

        }
        //fmt.printf("round: %d\n", round);
        //print_field(&f, width, height);
    }
    seats :=0;
    seats_2 :=0;
    for i:=0;i<width*height;i+=1
    {
        if f[i] == State.TAKEN { seats += 1; }
        if f[i+offset_1] == State.TAKEN { seats_2 += 1; }
    }
    fmt.println("Part1:", seats);
    fmt.println("Part2:", seats_2);
}

day10 :: proc ()
{
    fmt.printf("Day 10:\n");
    lines := load_input("input/10");
    //lines := load_input("input/10_test");
    //lines := load_input("input/10_test2");

    jolts := make([]int, len(lines));
    defer delete(jolts);
    jolts[0] = 0;
    for line,index in lines
    {
        //ignore last line
        if len(line) == 0 {continue;}
        jolts[index] = strconv.atoi(line);
    }
    sort.slice(jolts);
    for j,index in jolts
    {
        fmt.printf("%d: %d\n", index, j);
    }

    diff1 := 0;
    diff2 := 0;
    diff3 := 1;

    dd := 0;
    dd2 := 0;

    diff := 0;
    for i:=0; i<len(jolts)-1; i+=1
    {
        diff = jolts[i+1] - jolts[i];
        //fmt.printf("diff: %d", diff);
        switch diff
        {
            case 1:
                diff1+=1;
                if i+2 < len(jolts) && jolts[i+2] == jolts[i] + 2
                {
                    dd += 1;
                }
            case 2:
                diff2+=1;
            case 3:
                diff3+=1;
        }
    }
    fmt.printf("%d, %d, %d\n", diff1, diff2, diff3);
    fmt.printf("Part1: %d x %d = %d\n", diff1, diff3, diff1 * diff3);

    max := jolts[len(jolts)-1] + 3;
    fmt.printf("max: %d\n", max);
    fmt.printf("dd: %d\n", dd);
    fmt.printf("pow: %d\n", u64(math.pow(f32(dd-1),4)/2));

}

day9 :: proc ()
{
    fmt.printf("Day 9:\n");
    lines := load_input("input/9");
    pre :: 25;

    //lines := load_input("input/9_test");
    //pre :: 5;

    range : [pre]int;
    all := make([]int, len(lines));
    defer delete(all);

    part1 := 0;

    for i:=0;i<pre;i+=1
    {
        range[i] = strconv.atoi(lines[i]);
        all[i] = range[i];
    }

    valid : bool = false;
    current_value : int;
    last_index := 0;
    for i:=pre;i<len(lines);i+=1
    {
        valid = false;
        current_value = strconv.atoi(lines[i]);
        all[i] = current_value;
        loop: for x:=0;x<pre-1;x+=1
        {
            for y:=x+1;y<pre;y+=1
            {
                if current_value == range[x] + range[y]
                {
                    valid = true;
                    break loop;
                }
            }
        }

        if valid
        {
            range[i%pre] = current_value;
        }
        else
        {
            fmt.printf("Part1: %d\n", current_value);
            part1 = current_value;
            last_index = i;
            break;
        }
    }

    sum := 0;
    low := 0;
    high := 0;
    for i:=0; i<last_index; i+=1
    {
        sum = all[i];
        low = sum;
        high = sum;
        for f:=i+1; f<last_index; f+=1 {
            sum += all[f];
            if low > all[f] { low = all[f]; };
            if high < all[f] { high = all[f]; };
            //fmt.printf("sum: %d\n", sum);
            if sum == part1
            {
                //fmt.printf("Part2: %d, %d\n", low, high);
                fmt.printf("Part2: %d\n", low + high);
            }
            if sum > part1
            {
                break;
            }
        }

    }
}

day8 :: proc ()
{
    fmt.printf("Day 8:\n");
    lines := load_input("input/8");

    acc := 0;
    next_line := 0;

    line_used : [700]bool;
    loop := true;
    jmp_or_nop := 0;
    change := 0;
    loops := 0;

    for ;loop;
    {
        loops += 1;
        for i:=0; i<700; i+=1 {
            line_used[i] = false;
        }
        next_line = 0;
        loop = false;
        jmp_or_nop = 1;
        acc = 0;
        for ;next_line < len(lines)-1;
        {
            line := lines[next_line];
            if line_used[next_line] {
                if change == 0 {
                    fmt.printf("Part1: %d\n", acc);
                    change += 1;
                }
                loop = true;
                break;
            }
            if len(line) == 0 {continue;}
            parts := strings.split(line, " ");
            defer delete(parts);

            line_used[next_line] = true;
            value := strconv.atoi(parts[1]);
            switch parts[0] {
                case "acc":
                    acc += value;
                    next_line += 1;
                case "jmp":
                    if jmp_or_nop == change {
                        next_line += 1;
                        change += 1;
                        jmp_or_nop += 1;
                    }
                    else {
                        next_line += value;
                    }
                    jmp_or_nop += 1;
                case "nop":
                    if jmp_or_nop == change {
                        next_line += value;
                        change += 1;
                        jmp_or_nop += 1;
                    }
                    else {
                        next_line += 1;
                    }
                    jmp_or_nop += 1;
                case :
                    continue;
            }

        }
    }
    fmt.printf("Part2: %d\n", acc);
}

day7 :: proc ()
{
    rule :: struct
    {
        source : string,
        contains : [dynamic]string,
        contains_n : [dynamic]u32,
    };
    fmt.printf("Day 7:\n");
    lines := load_input("input/7");

    rules : [600]rule;

    for line,index in lines
    {
        parts := strings.split(line," ");
        if len(parts) == 1 {continue;}
        rules[index].source = fmt.aprintf("%s %s", parts[0], parts[1]);
        if len(parts) == 7 {continue;} // contains no other bag
        containing_bag := len(parts)/4-1;
        for i := 0; i < containing_bag ; i += 1 {
            append(&rules[index].contains, fmt.aprintf("%s %s", parts[5 + i * 4],
            parts[6 + i * 4]));
            append(&rules[index].contains_n, u32(strconv.atoi(parts[4 + i * 4])));
        }
    }

    gold : [dynamic]string;
    new : bool = true;
    indices : [600]bool;

    // part 1
    for ;new; {
        new = false;
        for r,index in rules {
            for c in r.contains {
                for g in gold {
                    if c == g && !indices[index] {
                        append(&gold, r.source);
                        new = true;
                        indices[index] = true;
                    }
                }
                if c == "shiny gold" && !indices[index]{
                    append(&gold, r.source);
                    new = true;
                    indices[index] = true;
                }
            }
        }
    }
    delete(gold);
    amount := 0;
    for i in indices {
        if i {
            amount += 1;
        }
    }
    fmt.printf("Part1: %d\n", amount);

    contains :: proc (rules: ^[600]rule, index : u32) -> u32 {
        if len(rules[index].contains) == 0 {return 0;}
        sum : u32 = 0;
        for r,i in rules {
            for subbag,sub_index in rules[index].contains
            {
                if subbag == r.source {
                    sum += rules[index].contains_n[sub_index] * (1 +
                    contains(rules, u32(i)));
                }
            }
        }
        return sum;
    }

    for r,index in rules {
        if r.source == "shiny gold" {
            fmt.printf("Part2: %d\n", contains(&rules, u32(index)));
        }
    }
    for r in rules {
        delete(r.contains_n);
        delete(r.contains);
    }
}


day6 :: proc ()
{
    // maybe use bit_set instead
    abc_to_bitmask :: proc (ch : rune) -> u32 {
        return u32(math.pow(2.0, f32(ch-97)));
    }

    count_bitmask_one :: proc(bitmask : u32) -> u32 {
        count : u32 = 0;
        for bm:u32=bitmask;bm>0;bm/=2 {
            if bm % 2 == 1 {
                count += 1;
            }
        }
        return count;
    }

    fmt.printf("Day 6:\n");
    lines := load_input("input/6");

    sum : u32 = 0;
    sum2 : u32 = 0;
    group : u32 = 0;
    group2 : u32 = 0;
    first : bool = true;
    yes_count_in_line : u32 = 0;
    for line in lines {
        if len(line) == 0 {
            sum += count_bitmask_one(group);
            sum2 += count_bitmask_one(group2);
            group = 0;
            group2 = 0;
            first = true;
            continue;
        }
        yes_count_in_line = 0;
        for ch in line {
            yes_count_in_line = yes_count_in_line | abc_to_bitmask(ch);
        }
        group = group | yes_count_in_line;
        if first {
            first = false;
            group2 = yes_count_in_line;
        }
        else
        {
            group2 = group2 & yes_count_in_line;
        }
    }
    fmt.printf("Part1: %d\n", sum);
    fmt.printf("Part2: %d\n", sum2);
}

day5 :: proc ()
{
    fmt.printf("Day 5:\n");
    lines := load_input("input/5");

    highest : u32 = 0;

    ids: [128*8]bool;
    for line, index in lines {
        if len(line) == 0 {continue};
        row : u32 = 0;
        for index := 0; index < 7; index += 1 {
            if line[index] == 'B' {
                row += u32(math.pow(2.0, f32(6-index)));
            }
        }
        col : u32 = 0;
        for index := 0; index < 3; index += 1 {
            if line[7+index] == 'R' {
                col += u32(math.pow(2.0, f32(2-index)));
            }
        }
        id : u32 = row * 8 + col;
        if id > highest {
            highest = id;
        }
        ids[id] = true;
    }
    fmt.printf("Part1: %d\n", highest);
    for i, index in ids {
        if index == 0 || index == 128 * 8 - 1 { continue; }
        if !i && ids[index-1] && ids[index+1] {
            fmt.printf("Part2: %d\n", index);
        }
    }
}

day4 :: proc ()
{
    fmt.printf("Day 4:\n");
    lines := load_input("input/4");

    byr: bool = false;
    iyr: bool = false;
    eyr : bool = false;
    hgt : bool = false;
    hcl : bool = false;
    ecl : bool = false;
    pid : bool = false;
    cid : bool = false;
    byr1: bool = false;
    iyr1: bool = false;
    eyr1 : bool = false;
    hgt1 : bool = false;
    hcl1 : bool = false;
    ecl1 : bool = false;
    pid1 : bool = false;
    cid1 : bool = false;

    valid : u32 = 0;
    valid1 : u32 = 0;
    for line in lines {
        if len(line) == 0 {
            if byr && iyr && eyr && hgt && hcl && ecl && pid {
                valid += 1;
            }
            if byr1 && iyr1 && eyr1 && hgt1 && hcl1 && ecl1 && pid1 {
                valid1 += 1;
            }
            byr = false;
            iyr = false;
            eyr = false;
            hgt = false;
            hcl = false;
            ecl = false;
            pid = false;
            cid = false;
            byr1 = false;
            iyr1 = false;
            eyr1 = false;
            hgt1 = false;
            hcl1 = false;
            ecl1 = false;
            pid1 = false;
            cid1 = false;
        }
        parts := strings.split(line, " ");
        for p in parts {
            if len(p) == 0 {continue};
            pair := strings.split(p,":");
            key := pair[0];
            value := pair[1];
            switch key {
                case "byr": {
                    byr1 = true;
                    v := strconv.atoi(value);
                    if v >= 1920 && v <= 2002 {
                        byr = true;
                    }
                }
                case "iyr": {
                    iyr1 = true;
                    v := strconv.atoi(value);
                    if v >= 2010 && v <= 2020 {
                        iyr = true;
                    }
                }
                case "eyr": {
                    eyr1 = true;
                    v := strconv.atoi(value);
                    if v >= 2020 && v <= 2030 {
                        eyr = true;
                    }
                }
                case "hgt": {
                    hgt1 = true;
                    if strings.has_suffix(value, "in") {
                        v:= strconv.atoi(strings.trim_suffix(value, "in"));
                        if v >= 59 && v <= 79 {
                            hgt = true;
                        }
                    }
                    if strings.has_suffix(value, "cm") {
                        v:= strconv.atoi(strings.trim_suffix(value, "cm"));
                        if v >= 150 && v <= 193 {
                            hgt = true;
                        }
                    }
                }
                case "hcl": {
                    hcl1 = true;
                    if strings.has_prefix(value, "#") {
                        v := strings.trim_prefix(value, "#");
                        hcl = true;
                        for r in v {
                            switch r {
                                case '0'..'9', 'a'..'f', 'A'..'F': continue;
                                case :
                                    hcl = false;
                                    break;
                            }

                        }
                    }
                }
                case "ecl": {
                    ecl1 = true;
                    switch value {
                        case "amb", "blu", "brn", "gry", "grn", "hzl", "oth":
                            ecl = true;
                    }
                }
                case "pid": {
                    pid1 = true;
                    if len(value) == 9 {
                        pid = true;
                    }
                }
                case "cid": {
                    cid1 = true;
                    cid = true;
                }
            }
        }

    }
    fmt.printf("Part1: %d\n", valid1);
    fmt.printf("Part2: %d\n", valid);
}

// note fifth is wrong i think but the result is the same
day3 :: proc ()
{
    fmt.printf("Day 3:\n");
    lines := load_input("input3");
    x : []u32 = {0, 0, 0, 0, 0};
    y : u32 = 0;
    trees : []u32 = {0, 0, 0, 0, 0};
    right : []u32 = {1, 3, 5, 7, 1};
    down : []u32 = {1, 1, 1, 1, 2};
    width : u32 = 0;
    for line in lines
    {
        // ignore empty lines
        if len(line) == 0 {continue;}
        if width == 0 {
            width = u32(len(line));
        }
        else
        {
            for i:=0;i<len(x);i+=1 {
                x[i] += right[i];
                if x[i] >= width
                {
                    x[i] -= width;
                }
                if y % down[i] == 0 && line[x[i]] == u8('#')
                {
                    trees[i] += 1;
                }
            }
        }
        y += 1;
    }
    fmt.printf("Part1: %d\n", trees[1]);
    q : u32 = 1;
    for t in trees
    {
        q = t * q;
        //fmt.printf("\ttree: %d\n", t);
    }
    fmt.printf("Part2: %d\n", q);
}

day2 :: proc ()
{
    fmt.printf("Day 2:\n");
    lines := load_input("input2");
    day2_part1(&lines);
    day2_part2(&lines);
}

day2_part1 :: proc (lines : ^[]string)
{
    valid : uint = 0;
    for line in lines
    {
        // ignore empty lines
        if len(line) == 0 {continue;}
        parts := strings.split(line, " ");
        range := strings.split(parts[0], "-");
        start := strconv.atoi(range[0]);
        end := strconv.atoi(range[1]);

        c := rune(parts[1][0]);
        password := parts[2];
        count := 0;

        for char in password {
            if char == c {
                count += 1;
            }
        }
        if count >= start && count <= end {
            valid += 1;
        }
    }

    fmt.printf("Part1: %d\n", valid);
}

test_d2p2 :: proc ()
{
    input := []string{"1-3 a: abcde", "1-3 b: cdefg", "2-9 c: ccccccccc"};
    day2_part2(&input);
}

day2_part2 :: proc (lines : ^[]string)
{
    valid := 0;
    for line in lines
    {
        // ignore empty lines
        if len(line) == 0 {continue;}

        parts := strings.split(line, " ");
        range := strings.split(parts[0], "-");
        start := strconv.atoi(range[0]);
        end := strconv.atoi(range[1]);
        c := parts[1][0];
        password := parts[2];

        count := 0;
        if password[start - 1] == c && password[end - 1] != c
        || password[start - 1] != c && password[end - 1] == c
        {
            valid += 1;
        }
    }
    fmt.printf("Part2: %d\n", valid);
}

day1 :: proc ()
{
    fmt.printf("Day 1:\n");
    lines := load_input("input1");
    day1_part1(&lines);
    day1_part2(&lines);
}

day1_part2 :: proc (lines : ^[]string) {
    for first := 0; first < len(lines); first += 1
    {
        v1 := strconv.atoi(lines[first]);
        for second := first+1; second < len(lines); second += 1
        {
            v2 := strconv.atoi(lines[second]);
            for third := second+1; third < len(lines); third += 1
            {
                v3 := strconv.atoi(lines[third]);
                if v3 == 0 {
                    continue;
                }

                if v1 + v2 + v3 == 2020
                {
                    fmt.printf("Part2: %d * %d * %d = %d\n", v1, v2,
                    v3, v1 * v2 * v3);
                }
            }
        }
    }
}

day1_part1 :: proc (lines : ^[]string ) {
    for i := 0; i < len(lines); i += 1
    {
        value := strconv.atoi(lines[i]);
        for next := i+1; next < len(lines); next += 1
        {
            next_value := strconv.atoi(lines[next]);
            if value + next_value == 2020
            {
                fmt.printf("Part1: %d * %d = %d\n", value, next_value, value *
                next_value);
            }
        }
    }
}
